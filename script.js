// 3-4
fetch('https://jsonplaceholder.typicode.com/todos')
.then((response) => response.json())
.then((json) => console.log(json.map((x) => x.title)));

// 5
fetch('https://jsonplaceholder.typicode.com/todos/1',)
.then(response => response.json())
.then(json => console.log(json));

// 6
fetch('https://jsonplaceholder.typicode.com/todos/1',)
.then(response => response.json())
.then(json => console.log(`The item "${json.title}" on the list has a status of ${json.completed} `));

// 7
fetch('https://jsonplaceholder.typicode.com/todos', {
	method: 'POST',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		completed: false,
		id: 201,
		title: "Created To Do List Item",
		userId: 1
	}),
})
.then((response) => response.json())
.then((json) => console.log(json));

// 8-9
fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: "PUT",
	headers: {
		"Content-Type" : "application/json",
	},
	body: JSON.stringify({
		dateCompleted: "Pending",
		description: "To update the my to do list with a different data structure",
		id: 1,
		status: "Pending",
		title: "Updated To Do List Item",
		userId: 1
	}),
})
.then((response) => response.json())
.then((json) => console.log(json));

// 10-11
fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: "PATCH",
	headers: {
		"Content-Type" : "application/json",
	},
	body: JSON.stringify({
		status: "Complete",
		dateCompleted: "07/09/21"
	}),
})
.then((response) => response.json())
.then((json) => console.log(json));

// 12
fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: "DELETE"
});

